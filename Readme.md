git clone https://bitbucket.org/crowdarautomation/gitlab_infra.git
cd gitlab_infra/
cd nginx/

cd ..
cd gitlab/
vi .env <- pegar las config
# https://hub.docker.com/r/gitlab/gitlab-ce/tags/
GITLAB_CE_VERSION=11.4.0-ce.0
# https://hub.docker.com/r/gitlab/gitlab-runner/tags/
GITLAB_RUNNER_VERSION=alpine-v11.4.0
POSTGRES_VERSION=9.6.8-alpine
REDIS_VERSION=3-alpine

NGINX_PROXY_NETWORK_NAME=nginx-proxy
GITLAB_HOST=gitlab.crowdaronline.com
REGISTRY_HOST=registry.crowdaronline.com
PAGES_HOST=gitlabpages.crowdaronline.com
GITLAB_SSH_IP=192.99.247.77
GITLAB_SSH_PORT=2222
GITLAB_TRUSTED_PROXY=172.20.0.0/16
LETSENCRYPT_EMAIL=ebaldizzoni@crowdaronline.com
TZ=America/Argentina

mkdir volume
mkdir volumes/config
vi volumes/config/gitlab.rb <- pegar contenido de https://github.com/mgcrea/docker-compose-gitlab-ce/blob/master/templates/gitlab.rb
sudo make
source .env
sudo docker-compose up -d
 
vi register-runner.sh <- agregar key del runner que se obtiene de gitlab
sudo sh register-runner.sh